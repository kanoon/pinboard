import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function MemberSelect(props) {
  return (
    <Autocomplete
      multiple
      id="tags-standard"
      options={props.users.map(p => ({value: p.uid, label: p.firstName + ' ' + p.lastName}))}
      disableCloseOnSelect
      value={props.members}
      getOptionLabel={(option) => option.label}
      getOptionSelected={(option, value) => option.label === value.label}
      renderOption={(option, { selected }) => (
        <React.Fragment>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ margin: 8 }}
            checked={selected}  
          />
          {option.label}
        </React.Fragment>
      )}
      style={{ marginTop: '0.4rem' }}
      onChange={(event, value) => props.handleSelect(event, value)}
      renderInput={(params) => (
        <TextField {...params} variant="standard" label="Members" placeholder="Members" />
      )}
    />
  );
}