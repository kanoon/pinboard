import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from "@material-ui/core/Grid";
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
  },
  cardRoot: {
    // maxWidth: 350,
    // width: "100%",
    // marginLeft: theme.spacing(1),
    // marginTop: '-0.5rem !important',
    // margin: '2.60rem',
    // textAlign: 'center',
  },
  cardDiv: {
    margin: theme.spacing(1.5),
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [expandedId, setExpandedId] = React.useState('');

  const handleExpandClick = (uid) => {
    setExpandedId(uid);
    setExpanded(!expanded);
  };

  const handleShowBoardData = (id) => {
    window.location.href = `/board/${id}`
  }

  if(props.boards.length === 0){
    return(
      <div style={{"textAlign": "center"}}>
        <p>No Boards Found.</p>
      </div>
    )
  }else{
    return (
      <React.Fragment>
        <Grid container spacing={3}>
        { props && props.boards && props.boards.map((board) => {
          return(
            <div className={classes.cardDiv}>
              <Grid item xs={12}>
                <Card className={classes.cardRoot}>
                  <CardHeader
                    avatar={
                      <Avatar aria-label="recipe" className={classes.avatar}>
                        {String(board.created_by.split(' ')[0]).toUpperCase().charAt(0) + 
                        String(board.created_by.split(' ')[1]).toUpperCase().charAt(0)}
                      </Avatar>
                    }
                    action={
                      <IconButton aria-label="settings">
                        <MoreVertIcon />
                      </IconButton>
                    }
                    title={board.title}
                    subheader={board.created_at ||"September 14, 2016"}
                  />
                  {/* <CardMedia
                    className={classes.media}
                    image="/static/images/cards/paella.jpg"
                    title="Paella dish"
                  /> */}
                  <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {board.desc}
                    </Typography>
                  </CardContent>
                  <CardActions disableSpacing>
                    <IconButton aria-label="edit" onClick={() => props.handleUpdate(board.uid)}>
                      <EditIcon />
                    </IconButton>
                    <IconButton aria-label="delete" onClick={() => props.handleDelete(board.uid)}>
                      <DeleteIcon />
                    </IconButton>
                    <IconButton aria-label="show" onClick={() => handleShowBoardData(board.uid)}>
                      <VisibilityIcon />
                    </IconButton>
                    <IconButton
                      className={clsx(classes.expand, {
                        [classes.expandOpen]: expandedId === board.uid ? expanded : false,
                      })}
                      onClick={() => handleExpandClick(board.uid)}
                      aria-expanded={expandedId === board.uid ? expanded : false}
                      aria-label="show more"
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </CardActions>
                  <Collapse in={ expandedId === board.uid ? expanded : false} timeout="auto" unmountOnExit>
                    <CardContent>
                      {/* <Typography paragraph>Method:</Typography>
                      */}
                      <Button onClick={() => props.handleShowMembers(board.uid)}>Show Members</Button>
                    </CardContent>
                  </Collapse>
                </Card>
              </Grid>
            </div>
          );
        })}
        </Grid>
      </React.Fragment>
    );
  }
}