import React, {useEffect} from 'react';
import MembersSelect from './MemberSelect';
import ShowBoards from './showBoards';
import {createBoard, getBoards, deleteBoard, udpateBoardData} from '../../actions/boards';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment'; 

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CssBaseline from '@material-ui/core/CssBaseline';
import CircularProgress from '@material-ui/core/CircularProgress';
import PersonIcon from '@material-ui/icons/Person';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { blue } from '@material-ui/core/colors';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  root: {
    display: 'flex',
  },
  button: {
    // margin: '0.5rem',
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing(3),
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

function Boards() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [openMembers, setOpenMembers] = React.useState(false);
  const [title, setTitle] = React.useState('');
  const [desc, setDesc] = React.useState('');
  const [members, setMembers] = React.useState([]);
  const [list, setList] = React.useState([]);
  const [boardId, setBoardId] = React.useState('');
  const [udpateBoard, setUpdateBoard] = React.useState(false);
  const dispatch = useDispatch();
  const boards = useSelector(state => state.boards.boards)
  const users = useSelector(state => state.boards.users)
  const user = useSelector(state => state.auth.user)


  useEffect(() => {
    dispatch(getBoards())
  }, []);

  const handleClickOpen = () => {
    setBoardId('')
    setTitle('')
    setDesc('')
    setMembers([])
    setUpdateBoard(false)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenMembers(false);
  };

  const handleSelect = (event, value) => {
    setMembers(value)
  };

  const handleCreate = (event) => {
    const currentUser = users.find((e) => e.uid === user.uid)
    const currentDate = moment().format('LLLL');
    dispatch(createBoard(title, desc, members, currentUser, currentDate))
    setOpen(false);
    event.preventDefault();
  };

  const handleDelete = (id) => {
    if(id !== null)
      dispatch(deleteBoard(id))
  }

  const handleUpdateClickOpen = (id) => {
    const selectedBoard = boards.find((e) => e.uid === id)
    setBoardId(selectedBoard.uid)
    setTitle(selectedBoard.title)
    setDesc(selectedBoard.desc)
    setMembers(selectedBoard.members)
    setUpdateBoard(true)
    setOpen(true);
  }

  const handleUpdate = (event) => {
    const currentUser = users.find((e) => e.uid === user.uid)
    const currentDate = moment().format('LLLL');
    dispatch(udpateBoardData(title, desc, members, boardId, currentUser, currentDate))
    setOpen(false)
    event.preventDefault();
  }

  const handleShowMembers = (id) => {
    const selectedBoard = boards.find((e) => e.uid === id)
    console.log(selectedBoard.members)
    setList(selectedBoard.members)
    setOpenMembers(true)
  }
  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleClickOpen}
              className={classes.button}
              startIcon={<AddCircleIcon />}
            >
              Add Board
            </Button>
          </Grid>
          <Grid item xs={12}>
            { boards ?
              <ShowBoards 
                boards={boards} 
                handleDelete={handleDelete}
                handleUpdate={handleUpdateClickOpen}
                handleShowMembers={handleShowMembers}
              /> : <div style={{"textAlign": "center"}}><CircularProgress /></div>
            }
          </Grid>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          { udpateBoard ? <DialogTitle id="form-dialog-title">Update Board</DialogTitle> : 
          <DialogTitle id="form-dialog-title">New Board</DialogTitle> }
          <DialogContent>
            <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send updates
              occasionally.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="title"
              label="Title"
              type="text"
              value={title}
              onChange={(event) => setTitle(event.target.value)}
              fullWidth
            />
            <TextField
              margin="dense"
              id="description"
              label="Description"
              type="text"
              value={desc}
              onChange={(event) => setDesc(event.target.value)}
              fullWidth
            />
            <MembersSelect handleSelect={handleSelect} users={users} members={members}/>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            { udpateBoard ?
              <Button onClick={handleUpdate} color="primary">
                Update
              </Button>
              :
              <Button onClick={handleCreate} color="primary">
                Create
              </Button>
            }
          </DialogActions>
        </Dialog>
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={openMembers}>
          <DialogTitle id="simple-dialog-title">List of Board Members</DialogTitle>
          <List>
            {list.map((member) => (
              <ListItem button key={member.label}>
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={member.label} />
              </ListItem>
            ))}
          </List>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
        </Grid>
      </Container>
    </div>
  );
}

export default Boards;
