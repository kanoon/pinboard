import React from 'react';
import {useSelector} from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import SubjectIcon from '@material-ui/icons/Subject';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';

function Title(props) {
  return (
    <Typography component="h2" variant="h6" color="primary" gutterBottom>
      {props.children}
    </Typography>
  );
}

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  members: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(0.3),
    },
  }
}));

export default function ShowBoardData() {
  const classes = useStyles();
  let url = window.location.pathname;
  let id = url.substring(url.lastIndexOf('/') + 1);
  const boards = useSelector(state => state.auth.boards)
  const board = boards && boards.find((e) => e.uid === id)

  const handleClick = (location) => {
    window.location.href = `/${location}`
  }

  return (
    <div className={classes.content}>
      {/* <div className={classes.appBarSpacer} /> */}
      <Container maxWidth="lg" className={classes.container}>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            startIcon={<KeyboardBackspaceIcon />}
            onClick={() => handleClick('boards')}
          >
            Back to Boards
          </Button>
          <Button
            variant="contained"
            color="primary"
            style={{"float": "right"}}
            className={classes.button}
            startIcon={<AddCircleIcon />}
            onChange={() => handleClick()}
          >
            Add Pin
          </Button>
        </Grid>
        { board === undefined ?
          <div style={{"textAlign": "center"}}><CircularProgress /></div>
          :
          <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={9}>
              <Typography variant="h2" gutterBottom style={{ fontWeight: 600 }}>
                {board && board.title}
              </Typography>
              <Chip
                icon={<FaceIcon />}
                style={{"maxWidth": "9rem"}}
                label={board && board.created_by}
                clickable
                color="primary"
              />
              <Divider style={{ 'marginTop' : '2rem' }}/>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={classes.paper}>
                <Title>Board Members</Title>
                <div className={classes.members}>
                  {board && board.members.map((e) => 
                    <Avatar>
                      {String(e.label.split(' ')[0]).toUpperCase().charAt(0) + 
                      String(e.label.split(' ')[1]).toUpperCase().charAt(0)}
                    </Avatar>
                  )}
                </div>
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2" color="textSecondary">
                <SubjectIcon /> For Developer Used
              </Typography>
            </Grid>
          </Grid>
        }
      </Container>
    </div>
  );
}