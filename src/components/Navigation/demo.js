import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {getData} from '../../actions/'
import { logoutUser } from "../../actions";
import * as ROUTES from '../../constants/routes';
import { Route, Redirect } from "react-router-dom";
import ProtectedRoute from "../ProtectedRoute";
import Home from "../Dashboard/Home";
import Login from "../Login";
import PasswordForgetPage from '../passwordForgot';
import SignUpPage from '../SignUp';
import Boards from '../Boards'
import ShowBoardData from '../Boards/ShowBoardData'
import ShowPinData from '../Pins/ShowPinData'
import Pins from '../Pins'
import Tags from '../Tags'

import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DeveloperBoardIcon from '@material-ui/icons/DeveloperBoard';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ListAltIcon from '@material-ui/icons/ListAlt';
import CommentIcon from '@material-ui/icons/Comment';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { deepOrange } from '@material-ui/core/colors';
import HomeIcon from '@material-ui/icons/Home';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  nonContent: {
    flexGrow: 1,
    // height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
}));

export default function MiniDrawer(props) {
  const {isVerifying, isAuthenticated} = props
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const menuId = 'primary-search-account-menu';
  const user = useSelector(state => state.auth.user)
  // const users = useSelector(state => state.auth.users)

  useEffect(() => {
    dispatch(getData())
  }, []);
 
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const doSignOut = () => {
    dispatch(logoutUser());
  }

  const handleListItemClick = (event, list) => {
    window.location.href = `/${list.charAt(0).toLowerCase() + list.slice(1)}`
  }

  // const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    isVerifying ? ( <div />) : isAuthenticated ? (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
          <Toolbar className={classes.toolbar}>
          <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
              <MenuIcon />
            </IconButton>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
              PinBoard
            </Typography>
            <Avatar className={classes.orange}>
              {String(user && user.email && user.email.split('@')[0]).toUpperCase().charAt(0) }
            </Avatar>
            <Button style={{"float": "right"}} color="inherit">
              {user.email}
            </Button>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={doSignOut}
              color="inherit"
            >
              <HighlightOffIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          open={open}
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <List>
            {['', 'Boards', 'Pins', 'Tags', 'Notes'].map((text, index) => (
              <ListItem button key={text} onClick={(event) => handleListItemClick(event, text)}>
                <ListItemIcon>{index === 0 ? <HomeIcon color="primary" /> : index === 1 ? <DashboardIcon color="primary" /> : index === 2 ? <DeveloperBoardIcon color="primary" /> : index === 3 ? <ListAltIcon color="primary" /> : index === 4 ? <CommentIcon color="primary" /> : null}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
            <ProtectedRoute
              exact
              path="/"
              component={Home}
              isAuthenticated={props.isAuthenticated}
              isVerifying={props.isVerifying}
            />
            <Route
              exact
              path={ROUTES.PASSWORD_FORGET}
              component={PasswordForgetPage}
            />
            <Route exact path="/login" component={Login} />
            <Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
            <Route exact path={ROUTES.BOARDS} component={Boards} />
            <Route exact path='/board/:id' component={ShowBoardData} />
            <Route exact path={ROUTES.PINS} component={Pins} />
            <Route exact path='/pin/:id' component={ShowPinData} />
            <Route exact path={ROUTES.TAGS} component={Tags} />
        </main>
      </div>
    )
    :
    (
      <div className={classes.grow}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
            >
              <MenuIcon />
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap >
              PinBoard
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.appBarSpacer} />
          <ProtectedRoute
            exact
            path="/"
            component={Home}
            isAuthenticated={props.isAuthenticated}
            isVerifying={props.isVerifying}
          />
          <Route
            exact
            path={ROUTES.PASSWORD_FORGET}
            component={PasswordForgetPage}
          />
          <Redirect to="/login" />
          <Route exact path="/login" component={Login} />
          <Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
          {/* <Route exact path={ROUTES.BOARDS} component={Boards} />
          <Route exact path={ROUTES.PINS} component={Pins} />
          <Route exact path={ROUTES.TAGS} component={Tags} /> */}
      </div>
    )
  );
}