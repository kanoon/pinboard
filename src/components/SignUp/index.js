import React, { Component } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import {connect} from 'react-redux'
import {signupUser} from '../../actions'

import { withStyles } from "@material-ui/styles";
import CssBaseline from '@material-ui/core/CssBaseline';
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';

const styles = () => ({
  "@global": {
    body: {
      backgroundColor: "#fff"
    }
  },
  paper: {
    // marginTop: 50,
    display: "flex",
    padding: 20,
    flexDirection: "column",
    alignItems: "center",
    marginTop: 50,
  },
  avatar: {
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "#f50057"
  },
  // form: {
  //   marginTop: 1
  // },
  errorText: {
    color: "#f50057",
    marginBottom: 5,
    textAlign: "center"
  },
  grid: {
    marginTop: '1.2rem'
  }
});

const SignUpPage = (props) => (
  <div>
    {/* <h1>SignUp</h1> */}
    <SignUpForm {...props} />
  </div>
);

const INITIAL_STATE = {
  firstName: '',
  lastName: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { dispatch } = this.props;
    const { firstName, lastName, email, passwordOne } = this.state;

    dispatch(signupUser(firstName, lastName, email, passwordOne, this.props));
    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const {
      firstName, 
      lastName,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;
    const {classes, signinError, isAuthenticated} = this.props

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      firstName === '' || 
      lastName === '';

    
    if (isAuthenticated && signinError == null) {
      return <Redirect to="/" />;
    }else{
      return (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          { signinError && signinError.message &&
            <Alert severity="error">{signinError.message}</Alert>
          }
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign Up
            </Typography>
            <form onSubmit={this.onSubmit} className={classes.form} noValidate>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="firstName"
                    label="First Name"
                    value={firstName}
                    onChange={this.onChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="lastName"
                    label="Last Name"
                    value={lastName}
                    onChange={this.onChange}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="email"
                    value={email}
                    label="Email"
                    onChange={this.onChange}
                    type="text"
                    placeholder="Email Address"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="passwordOne"
                    value={passwordOne}
                    onChange={this.onChange}
                    type="password"
                    label="Password"
                    placeholder="Password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="passwordTwo"
                    value={passwordTwo}
                    onChange={this.onChange}
                    type="password"
                    label="Confirm Password"
                    placeholder="Confirm Password"
                  />
                </Grid>
              </Grid>
              <Button
                fullWidth
                variant="contained"
                color="primary"
                disabled={isInvalid} 
                className={classes.submit}
                type="submit">
                Sign Up
              </Button>
              <Grid className={classes.grid} container>
                <Grid item xs>
                <p>
                  <Link to={ROUTES.SIGN_IN}>Already have an account?</Link>
                </p>
                </Grid>
              </Grid>
              {error && <p>{error.message}</p>}
              </form>
          </Paper>
        </Container>
      );
    }
  }
}

const SignUpLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignUpForm = withRouter(SignUpFormBase);

const mapStateWithState = (state) => {
  return {
    isSigningIn: state.auth.isLoggingIn,
    singinError: state.auth.loginError,
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user,
    signinError: state.auth.signinError
  }
} 

export default withStyles(styles)(connect(mapStateWithState)(SignUpPage));

export { SignUpForm, SignUpLink };
