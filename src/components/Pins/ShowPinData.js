import React from 'react';
import clsx from 'clsx';
import {useSelector} from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import DashboardIcon from '@material-ui/icons/Dashboard';
import EventNoteIcon from '@material-ui/icons/EventNote';

function Title(props) {
  return (
    <Typography component="h2" variant="h6" color="primary" gutterBottom>
      {props.children}
    </Typography>
  );
}

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  spacing:{
    marginTop: theme.spacing(4)
  },
  button: {
    borderRadius: "5em"
  },
  members: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(0.3),
    },
  }
}));

export default function ShowBoardData() {
  const classes = useStyles();
  let url = window.location.pathname;
  let id = url.substring(url.lastIndexOf('/') + 1);
  const pins = useSelector(state => state.auth.pins)
  const boards = useSelector(state => state.auth.boards)
  const pin = pins && pins.find((e) => e.uid === id)
  const pinBoard = boards && boards.find((e => e.uid === pin.boardId))

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  const handleClick = (location) => {
    window.location.href = `/${location}`
  }

  return (
    <div>
      {/* <div className={classes.appBarSpacer} /> */}
      <Container maxWidth="lg" className={classes.container}>
        { pin === undefined ?
            <div style={{"textAlign": "center"}}><CircularProgress /></div>
          :
          <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={9} className={classes.fixedHeight}>
              <Button
                variant="contained"
                color="default"
                className={classes.button}
                startIcon={<KeyboardBackspaceIcon />}
                onClick={() => handleClick('pins')}
              >
                Back to Pins
              </Button>
              <Grid item xs={12} className={classes.spacing}>
                <Typography variant="h2" gutterBottom style={{ fontWeight: 600 }}>
                  {pin && pin.title}
                </Typography>
                <div className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={3}>
                      <Paper className={classes.paper}>
                        <Grid container spacing={2}>
                          <Grid item xs={2}>
                            <DashboardIcon color="primary" />
                          </Grid>
                          <Grid item xs={10}>
                            <Typography>{pinBoard && pinBoard.title}</Typography>
                          </Grid>
                        </Grid>
                      </Paper>
                    </Grid>
                    <Grid item xs={3}>
                      <Paper className={classes.paper}>
                        <Grid container spacing={2}>
                          <Grid item xs={2}>
                            <EventNoteIcon color="primary" />
                          </Grid>
                          <Grid item xs={10}>
                            <Typography>{pin.created_at}</Typography>
                          </Grid>
                        </Grid>
                      </Paper>
                    </Grid>
                  </Grid>
                </div>
                <Divider style={{ 'marginTop' : '2rem' }}/>
              </Grid>
              <Grid item xs={12} className={classes.spacing}>
                <Paper className={fixedHeightPaper}>
                  {pin.content}
                </Paper>
              </Grid>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Grid item xs={12}>
                <Title>Author</Title>
                <div className={classes.members}>
                  <Chip
                    icon={<FaceIcon />}
                    label={pin && pin.created_by}
                    clickable
                    color="primary"
                  />
                </div>
              </Grid>
              <Grid item xs={12} className={classes.spacing}>
                <Title>Contributors</Title>
                <div className={classes.members}>
                  {pin && pin.contributors.map((e) => 
                    <Avatar>
                      {String(e.label.split(' ')[0]).toUpperCase().charAt(0) + 
                      String(e.label.split(' ')[1]).toUpperCase().charAt(0)}
                    </Avatar>
                  )}
                </div>
              </Grid>
              <Grid item xs={12} className={classes.spacing}>
                <Title>Tags</Title>
                <div className={classes.members}>
                  {pin && pin.tags.map((e) => 
                    <Chip
                      label={e.label}
                      clickable
                      color="primary"
                    />
                  )}
                </div>
              </Grid>
            </Grid>
          </Grid>
        }
      </Container>
    </div>
  );
}