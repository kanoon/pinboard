import React, { useRef, useEffect } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import TextField from '@material-ui/core/TextField';

function TextEditor(props){
  return (
    <Editor
      wrapperClassName="toolbarClassName"
      editorClassName=""
      onChange={(event) => props.handleTextChange(event)}
      value={props.content} 
      toolbar={{
        inline: { inDropdown: true },
        list: { inDropdown: true },
        textAlign: { inDropdown: true },
        link: { inDropdown: true },
        history: { inDropdown: true },
      }}
    />
  )
}

export default TextEditor;