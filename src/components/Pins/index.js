import React, {useEffect, useMemo} from 'react';
import ContributersSelect from './contributersSelect';
import ShowPins from './showPins';
import {getPins, createPin, udpatePinData, deletePin} from '../../actions/pins';
import {useSelector, useDispatch} from 'react-redux';
import {useDropzone} from 'react-dropzone'
import moment from 'moment'; 
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import CssBaseline from '@material-ui/core/CssBaseline';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { blue } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  root: {
    // marginLeft: '5rem',
    // marginTop: '1.5rem',
    // marginBottom: '1.5rem',
  },
  button: {
    marginLeft: theme.spacing(3),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  search: {
    // marginLeft: '0.5rem',
    // marginTop: '1.5rem'
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

function Pins() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState('');
  const [content, setContent] = React.useState('');
  const [pinTags, setPinTags] = React.useState([]);
  const [contributers, setContributers] = React.useState([]);
  const [pinboard, setPinboard] = React.useState([]);
  const [userOptions, setUserOptions] = React.useState([]);
  const [search, setSearch] = React.useState('');
  const [switchMode, setSwitchMode] = React.useState(true);
  const [udpatePin, setUdpatePin] = React.useState(false);
  const [pinId, setPinId] = React.useState(false);
  const [command, setCommand] = React.useState('');
  const [boardId, setBoardId] = React.useState('');
  const [clickedButton, setClickedButton] = React.useState('');
  const [errorModal, setErrorModal] = React.useState(false);
  const dispatch = useDispatch();
  const boards = useSelector(state => state.pins.boards)
  const tags = useSelector(state => state.pins.tags)
  const users = useSelector(state => state.pins.users)
  const user = useSelector(state => state.auth.user)
  const pins = useSelector(state => state.pins.pins)

  useEffect(() => {
    dispatch(getPins())
  }, []);

  const handleClickOpen = (id) => {
    console.log(boards)
    if(boards === undefined || boards.length === 0){
      setErrorModal(true)
    }else {
      // console.log(pinboard.length != 0 ? userOptions : users.map((e) => ({value: e.uid, label: e.title}))
      setPinId('')
      setTitle('')
      setContent('')
      setCommand('')
      setContributers([])
      setPinTags([])
      setPinboard([])
      setUdpatePin(false)
      setClickedButton(id)
      setOpen(true);
    }
  };

  const handleClose = () => {
    setOpen(false);
    setErrorModal(false);
  };

  const handleSelect = (event, id, value) => {
    const selectedBoard = boards.find((e) => e.uid === value.value)
    if(id === "board"){
      setPinboard(value)
      setBoardId(selectedBoard.uid)
      setUserOptions(selectedBoard)
    }else if(id === "contributers") {
      setContributers(value)
    }else {
      setPinTags(value)
    }
  };

  const handleCreate = (event) => {
    const currentUser = users.find((e) => e.uid === user.uid)
    const currentDate = moment().format('LL');
    dispatch(createPin(title, content, command, pinboard, boardId, pinTags, contributers, clickedButton, currentUser, currentDate))
    setOpen(false);
    event.preventDefault();
  };

  const handleDelete = (id) => {
    dispatch(deletePin(id))
  }

  const handleUpdateClickOpen = (event, id) => {
    const selectedPin = pins.find((e) => e.uid === id)
    // const board = boards.find((e) => e.uid === selectedPin.board.value)
    // setPinboard(selectedPin.board)
    setPinId(selectedPin.uid)
    setTitle(selectedPin.title)
    setContent(selectedPin.content)
    setContributers(selectedPin.contributors)
    setPinTags(selectedPin.tags)
    setClickedButton(selectedPin.type)
    setUdpatePin(true)
    setOpen(true);
  }

  const handleUpdate = (event) => {
    const currentUser = users.find((e) => e.uid === user.uid)
    const currentDate = moment().format('LL');
    dispatch(udpatePinData(title, content, command, pinboard, boardId, pinTags, contributers, clickedButton, currentUser, currentDate, pinId))
    setOpen(false);
    event.preventDefault();
  }

  const handleSwitch = (event) => {
    setSwitchMode(event.target.checked)
  }

  const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#3f51b5',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    marginTop: '2.5rem',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out'
  };
  
  const activeStyle = {
    borderColor: '#2196f3'
  };
  
  const acceptStyle = {
    borderColor: '#00e676'
  };
  
  const rejectStyle = {
    borderColor: '#ff1744'
  };

  const getFilesFromEvent = (event) => {
    console.log('##################')
    console.log(event.target.files[0])
    console.log('##################')
  }

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({accept: 'text/plain', getFilesFromEvent: (event) => getFilesFromEvent(event)});

  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isDragActive,
    isDragReject,
    isDragAccept
  ]);
  
  // const files = acceptedFiles.map(file => (
  //   <li key={file.path}>
  //     {file.path} - {file.size} bytes
  //   </li>
  // ));

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => handleClickOpen("Snippet")}
              // className={classes.button}
              startIcon={<AddCircleIcon />}
            >
              ADD SNIPPET
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={()  => handleClickOpen("Component")}
              className={classes.button}
              startIcon={<AddCircleIcon />}
            >
              ADD COMPONENT
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => handleClickOpen("Task")}
              className={classes.button}
              startIcon={<AddCircleIcon />}
            >
              ADD TASK LIST
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => handleClickOpen("Note")}
              className={classes.button}
              startIcon={<AddCircleIcon />}
            >
              ADD NOTE
            </Button>
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin="dense"
              id="title"
              label="Filter Pins by Tags"
              type="text"
              className={classes.search}
              value={search}
              onChange={(event) => setSearch(event.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            { pins ?
              <ShowPins 
                pins={pins}
                handleShow={handleUpdateClickOpen}
                handleDelete={handleDelete}
              /> : <div style={{"textAlign": "center"}}><CircularProgress /></div>
            }
          </Grid>
          <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
              <Toolbar>
                <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                  <CloseIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  { udpatePin ? `Update Code ${clickedButton}` : `Add Code ${clickedButton}` }
                </Typography>
                { udpatePin ? 
                  <Button autoFocus color="inherit" onClick={handleUpdate}>
                    Update
                  </Button>
                  :
                  <Button autoFocus color="inherit" onClick={handleCreate}>
                    save
                  </Button>
                }
              </Toolbar>
            </AppBar>
            <Container maxWidth="sm" style={{ backgroundColor: 'white', height: '50vh' }}>
              <FormControlLabel
                className={classes.formControlLabel}
                control={<Switch checked={switchMode} onChange={(event) => handleSwitch(event)}/>}
                label="Switch View"
              />
              <TextField
                autoFocus
                margin="dense"
                id="title"
                label="Title"
                type="text"
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                fullWidth
              />
              <TextField
                margin="dense"
                id="content"
                label="Content"
                type="text"
                value={content}
                onChange={(event) => setContent(event.target.value)}
                fullWidth
              />
              <ContributersSelect
                title="Tags"
                id="tags"
                userOptions={tags.map((e) => ({value: e.uid, label: e.name.name}))}
                handleSelect={handleSelect}
                value={pinTags}
              />
              <Autocomplete
                options={boards && boards.map((e) => ({value: e.uid, label: e.title}))}
                getOptionLabel={(option) => option.label}
                id="board"
                // value={pinboard}
                getOptionSelected={(option, value) => { return option.title === value.title }}
                onChange={(event, value) => handleSelect(event, 'board', value)}
                renderInput={(params) => <TextField {...params} label="Board" margin="normal" />}
              />
              { clickedButton === "Component" &&
                <TextField
                  margin="dense"
                  id="command"
                  label="Compoenent Installer Command"
                  type="text"
                  value={command}
                  onChange={(event) => setCommand(event.target.value)}
                  fullWidth
                />
              }
              <ContributersSelect
                title="Contributers"
                id="contributers" 
                userOptions={pinboard.length !== 0 ? userOptions.members : users.map((e) => ({value: e.uid, label: e.firstName}))} 
                handleSelect={handleSelect}
                value={contributers}
              />
              { clickedButton === "Component" &&
                <div className="container">
                  <div {...getRootProps({style})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                  </div>
                </div>
              }
              {/* <TextEditor 
                content={content}
                handleTextChange={handleTextChange}
              /> */}
            </Container>
          </Dialog>
          <Snackbar open={errorModal} autoHideDuration={4000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              No Boards Found.
            </Alert>
          </Snackbar>
        </Grid>
      </Container>
    </div>
  );
}

export default Pins;
