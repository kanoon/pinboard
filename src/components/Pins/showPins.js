import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from "@material-ui/core/Grid";
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeveloperBoardIcon from '@material-ui/icons/DeveloperBoard';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  cardRoot: {
    // maxWidth: 350,
    width: "100%",
    marginTop: '1.5rem !important',
    // margin: '0.5rem',
    // textAlign: 'center',
  },
  cardDiv: {
    margin: theme.spacing(1.5),
  },
  mediaRoot: {
    // marginLeft: '1rem',
    // fontSize: '15rem'
    textAlign: 'center',
    background: '#F0F0F0'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard(props) {
  const classes = useStyles();
  // const [expanded, setExpanded] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [expandedId, setExpandedId] = React.useState('');
  // const [selectedPin, setSelectedPin] = React.useState([]);

  // const handleExpandClick = (uid) => {
  //   setExpandedId(uid);
  //   setExpanded(!expanded);
  // };

  const handleClick = (event, id) => {
    setExpandedId(id);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setExpandedId(null);
    setAnchorEl('');
  };

  const handleDelete = () => {
    console.log(expandedId)
    props.handleDelete(expandedId)
    setExpandedId(null);
    setAnchorEl('');
  }

  const handleShowData = () => {
    window.location.href = `/pin/${expandedId}`
  }

  if(props.pins.length === 0){
    return(
      <div style={{"textAlign": "center"}}>
        <p>No Pins Found</p>
      </div>
    )
  }else{
    return (
      <React.Fragment>
        <Grid container spacing={3}>
        { props && props.pins && props.pins.map((pin) => {
          return(
            <div className={classes.cardDiv}>
              <Grid item xs={12}>
                <Card className={classes.cardRoot}>
                  <CardHeader
                    action={
                      <IconButton aria-label="settings">
                        <MoreVertIcon onClick={(event) => handleClick(event, pin.uid)}/>
                      </IconButton>
                    }
                    title={pin.title}
                    subheader={
                      <React.Fragment>
                        <Chip variant="default" size="small" label={pin.created_at} />
                        &nbsp;
                        <Chip variant="default" color="primary" size="small" label={pin.type} />
                      </React.Fragment>
                    }
                  />
                  <div className={classes.mediaRoot} onClick={(event) => props.handleShow(event, pin.uid)}>
                    { pin.type === "Snippet" &&
                      <DeveloperBoardIcon color="primary" style={{"fontSize": "12rem"}}/>
                    }
                    { pin.type === "Component" &&
                      <NoteAddIcon color="primary" style={{"fontSize": "12rem"}}/>
                    }
                    { pin.type === "Task" &&
                      <LibraryAddIcon color="primary" style={{"fontSize": "12rem"}}/>
                    }
                    { pin.type === "Note" &&
                      <CreateNewFolderIcon color="primary" style={{"fontSize": "12rem"}}/>
                    }
                  </div>
                </Card>
              </Grid>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleShowData}>show</MenuItem>
                <MenuItem onClick={handleDelete}>Delete</MenuItem>
              </Menu>
            </div>
          );
        })}
        </Grid>
      </React.Fragment>
    );
  }
}