import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

// import * as ROUTES from '../../constants/routes';
import {connect} from 'react-redux';
import {forgotPassword} from '../../actions'

// const PasswordForgetPage = () => (
//   <div>
//     <h1>PasswordForget</h1>
//   </div>
// );

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email } = this.state;
    const { dispatch } = this.props;
    dispatch(forgotPassword(email));
    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, error } = this.state;
    const { linkReceived } = this.props.passwordRequest;

    console.log(this.props)

    const isInvalid = email === '';

    return (
      <React.Fragment>
        <form onSubmit={this.onSubmit}>
          <input
            name="email"
            value={this.state.email}
            onChange={this.onChange}
            type="text"
            placeholder="Email Address"
          />
          <button disabled={isInvalid} type="submit">
            Reset My Password
          </button>
          
          <p>{linkReceived && "New Password Request link is send to your given email id."}</p>
          <p>{this.props.passwordRequest.error && this.props.passwordRequest.error.message}</p>
          {error && <p>{error.message}</p>}
        </form>
        <button onClick={() => this.props.history.push("/")}>
            Back
        </button>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      isLoggingIn: state.auth.isLoggingIn,
      loginError: state.auth.loginError,
      isAuthenticated: state.auth.isAuthenticated,
      passwordRequest: state.auth.passwordRequest,
    };
  }

export default connect(mapStateToProps)(PasswordForgetFormBase);

