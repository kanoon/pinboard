import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Title from './Title';
import DeveloperBoardIcon from '@material-ui/icons/DeveloperBoard';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';


const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export default function Actions() {
  const classes = useStyles();
  // const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <React.Fragment>
      <Title>Smart Actions</Title>
      <Grid container spacing={3}>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={classes.paper}>
            Pin a Code Snippet
            <DeveloperBoardIcon color="primary" style={{"fontSize": "10rem"}}/>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={classes.paper}>
            Pin a Component
            <NoteAddIcon color="primary" style={{"fontSize": "10rem"}}/>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={classes.paper}>
            Pin a Task List
            <LibraryAddIcon color="primary" style={{"fontSize": "10rem"}}/>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Paper className={classes.paper}>
            Pin a Note
            <CreateNewFolderIcon color="primary" style={{"fontSize": "10rem"}}/>
          </Paper>
        </Grid>
      </Grid>
      {/* <div className={classes.seeMore}>
        <Link color="primary" href="#">
          See more orders
        </Link>
      </div> */}
    </React.Fragment>
  );
}