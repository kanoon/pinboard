import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer, Label } from 'recharts';
import Title from './Title';
import _ from 'lodash';

export default function Chart(props) {
  const theme = useTheme();
  const data1 = props.pins && props.pins.map((e) => e.board)
  var result=[];
  _.forIn(_.countBy(data1, "label"), function(value, key) {
      result.push({ "name": key, "pins": value });
  });
  return (
    <React.Fragment>
      <Title>Pins Per Board</Title>
      <ResponsiveContainer>
        <LineChart
          data={result}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        > 
          <Line type="monotone" dataKey="pins" stroke="#8884d8" />
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <Tooltip />
          <XAxis dataKey="name" stroke={theme.palette.text.secondary} />
          <YAxis stroke={theme.palette.text.secondary}>
            <Label
              angle={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              Pins
            </Label>
          </YAxis>
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}