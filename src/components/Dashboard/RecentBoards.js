import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';
import moment from 'moment'; 
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  depositContext: {
    flex: 1,
  },
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  loading: {
    marginTop: "20%",
    textAlign: 'center'
  }
}));

export default function RecentBoards(props) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Recent Board Status</Title>
      <Typography component="p" variant="h4">
        {props.boards && props.boards.length} Boards
      </Typography>
      <Typography color="textSecondary" className={classes.depositContext}>
        {moment().format('MMMM Do YYYY, h:mm:ss a')}
      </Typography>
      <div>
      <Button href="/boards" color="primary">
        View All Boards
      </Button>
      </div>
    </React.Fragment>
  );
}