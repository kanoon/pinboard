  import React from 'react';
  import _ from 'lodash';
  import ListItem from '@material-ui/core/ListItem';
  import ListItemIcon from '@material-ui/core/ListItemIcon';
  import ListItemText from '@material-ui/core/ListItemText';
  import List from '@material-ui/core/List';
  import Divider from '@material-ui/core/Divider';
  import Title from './Title';
  import AccountCircleIcon from '@material-ui/icons/AccountCircle';


  export default function TopContributors (props) {
    const contributors = props.pins && props.pins.map((e) => e.contributors)
    let mergedContributors = [].concat.apply([], contributors);
    var result=[];
    _.forIn(_.countBy(mergedContributors, "label"), function(value, key) {
        result.push({ "value": key, "count": value });
    });
    const lists = _.takeRight(_.sortBy(result, 'count'), 2)
    return(
      <React.Fragment>
        <Title>Top Contributors</Title>
        <List dense={true}>
          { _.reverse(lists).map((list) => { 
            return (
              <div>
                <ListItem button>
                  <ListItemIcon>
                    <AccountCircleIcon color="primary" />
                  </ListItemIcon>
                  <ListItemText primary={list[Object.keys(list)[0]]} />
                </ListItem>
                <Divider />
              </div>
            )
          })}
        </List>
      </ React.Fragment>
    );
  }
