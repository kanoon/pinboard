import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { connect } from "react-redux";
import Navigation from './components/Navigation/demo';


function App(props) {
  const { isAuthenticated, isVerifying } = props;
  return (
    <Router>
      <Switch>
        <div>
          <Navigation isAuthenticated={isAuthenticated} isVerifying={isVerifying} />
        </div>
      </Switch>
    </Router>
  );
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    isVerifying: state.auth.isVerifying
  };
}

export default connect(mapStateToProps)(App);
