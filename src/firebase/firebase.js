import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCW1RckAAs4ImfEVir4oTJ_IBZLruatKs4",
  authDomain: "react-firebase-c73ba.firebaseapp.com",
  databaseURL: "https://react-firebase-c73ba.firebaseio.com",
  projectId: "react-firebase-c73ba",
  storageBucket: "react-firebase-c73ba.appspot.com",
  messagingSenderId: "611552377687",
  appId: "1:611552377687:web:24c88bf5e7f2df56e4dc12",
  measurementId: "G-43SLHLB8H7"
};

export const myFirebase = firebase.initializeApp(firebaseConfig);
export const storage = firebase.storage();
const baseDb = myFirebase.firestore();

export const db = baseDb;
