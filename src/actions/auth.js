import { myFirebase } from "../firebase/firebase";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const USER_CREATED_SUCCESSFULLY = "USER_CREATED_SUCCESSFULLY";
export const USER_CREATION_FAILURE = "USER_CREATION_FAILURE";

export const PASSWORD_REQUEST_SUCCESS = "PASSWORD_REQUEST_SUCCESS";
export const PASSWORD_REQUEST_FAILURE = "PASSWORD_REQUEST_FAILURE";

export const VERIFY_REQUEST = "VERIFY_REQUEST";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";

export const GET_DATA = "GET_DATA";

const requestLogin = () => {
  return {
    type: LOGIN_REQUEST
  };
};

const receiveLogin = user => {
  return {
    type: LOGIN_SUCCESS,
    user
  };
};

const loginError = () => {
  return {
    type: LOGIN_FAILURE
  };
};

const requestLogout = () => {
  return {
    type: LOGOUT_REQUEST
  };
};

const receiveLogout = () => {
  return {
    type: LOGOUT_SUCCESS
  };
};

const logoutError = () => {
  return {
    type: LOGOUT_FAILURE
  };
};

const verifyRequest = () => {
  return {
    type: VERIFY_REQUEST
  };
};

const verifySuccess = () => {
  return {
    type: VERIFY_SUCCESS
  };
};

const forgotPasswordRequest = (email) => {
  return {
    type: PASSWORD_REQUEST_SUCCESS,
    email: email
  }
}

const forgotPasswordFailure = (email, error) => {
  return {
    type: PASSWORD_REQUEST_FAILURE,
    email: email,
    error: error
  }
}

const newUserCreated = (user) => {
  return {
    type: USER_CREATED_SUCCESSFULLY,
    user
  }
}

const newUserCreationFailure = (error) => {
  return {
    type: USER_CREATION_FAILURE,
    error: error
  }
}

const getAppData = (data) => {
  return {
    type: GET_DATA,
    data: data
  }
}

export const loginUser = (email, password) => dispatch => {
  dispatch(requestLogin());
  myFirebase.database().ref('/').on('value', snapShot => { console.log(snapShot.val())})
  myFirebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => {
      dispatch(receiveLogin(user));
    })
    .catch(error => {
      //Do something with the error if you want!
      dispatch(loginError());
    });
};

export const signupUser = (firstName, lastName, email, password, props) => dispatch => {
  // dispatch(requestLogin());
  myFirebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(authUser => {
      myFirebase.database().ref(`users/${authUser.user.uid}`)
      .set({
        firstName,
        lastName,
        email,
      })
      .then((user) => {
        dispatch(newUserCreated(authUser.user));
        props.history.push('/');
      })
      .catch(error => {
        dispatch(newUserCreationFailure(error));
      });
    })
    .catch(error => {
      dispatch(newUserCreationFailure(error));
      //Do something with the error if you want!
    });
};

export const logoutUser = () => dispatch => {
  dispatch(requestLogout());
  myFirebase
    .auth()
    .signOut()
    .then(() => {
      dispatch(receiveLogout());
    })
    .catch(error => {
      //Do something with the error if you want!
      dispatch(logoutError());
    });
};

export const verifyAuth = () => dispatch => {
  dispatch(verifyRequest());
  myFirebase.auth().onAuthStateChanged(user => {
    if (user !== null) {
      dispatch(receiveLogin(user));
    }
    dispatch(verifySuccess());
  });
};

export const forgotPassword = (email) => dispatch => {
  myFirebase.auth().sendPasswordResetEmail(email) 
  .then(() => {
    dispatch(forgotPasswordRequest(email));
  })
  .catch((error) => {
    dispatch(forgotPasswordFailure(email, error));
  })
}

export const getData = () => dispatch => {
  myFirebase.database().ref('/').on('value', snapShot => {
    const boardsObject = snapShot.val().boards
    const pinsObject = snapShot.val().pins
    const usersObject = snapShot.val().users
    const tagsObject = snapShot.val().tags
    const boardsList = boardsObject && Object.keys(boardsObject).map(key => ({
      ...boardsObject[key],
      uid: key,
    }));
    const pinsList = pinsObject && Object.keys(pinsObject).map(key => ({
        ...pinsObject[key],
        uid: key,
      }));
    const tagsList = Object.keys(tagsObject).map(key => ({
        ...tagsObject[key],
        uid: key,
      }));
    const usersList = Object.keys(usersObject).map(key => ({
      ...usersObject[key],
      uid: key,
    }));
    const data = { boards: boardsList, pins: pinsList, tags: tagsList, users: usersList }
    dispatch(getAppData(data))
  })
}
