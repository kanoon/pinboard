import { myFirebase } from "../firebase/firebase";

export const getTags = () => dispatch => {
  myFirebase.database().ref('/').on('value', snapShot => {
    const tagsObject = snapShot.val().tags
    const usersObject = snapShot.val().users
    const tagsList = tagsObject && Object.keys(tagsObject).map(key => ({
      ...tagsObject[key],
      uid: key,
    }));
    const usersList = Object.keys(usersObject).map(key => ({
      ...usersObject[key],
      uid: key,
    }));
    dispatch({type: 'GET_TAGS', data: {tags: tagsList, users: usersList}})
  })
}

export const createTag = (name, currentUser, currentDate) => dispatch => {
  myFirebase.database().ref('tags').push({
    name: name,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'CREATE_TAG', pin: response})
  })
}

export const udpateTagData = (id, name, currentUser, currentDate) => dispatch => {
  myFirebase.database().ref(`pins`).on('value', snapShot => {
    snapShot.forEach(item => {
      const otherPinsTagList = (item.val().tags) ? item.val().tags.filter(tag => (tag.value !== id)) : null
      const selectedPinsTagList = (item.val().tags) ? item.val().tags.filter(tag => (tag.value === id)) : null
      updatePinsTagName(otherPinsTagList, selectedPinsTagList, item.key, name, id)
    })
  })
  myFirebase.database().ref(`tags/${id}`).set({
    name: name,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'UPDATE_TAG'})
  })
}

export const deleteTag = (id) => dispatch => {
  myFirebase.database().ref(`pins`).on('value', snapShot => {
    snapShot.forEach(item => {
      const udpatePinsTagList = (item.val().tags) ? item.val().tags.filter(tag => (tag.value !== id)) : null
      udpatePinsTag(udpatePinsTagList, item.key)
    })
  })
  myFirebase.database().ref(`tags/${id}`).remove()
  .then((res) => dispatch({type: 'DELETE_TAG', id: id}))
}

const udpatePinsTag =( list, key ) => {
  myFirebase.database().ref(`pins/${key}`).update({
    tags: list
  })
}

const updatePinsTagName = (otherList, changedlist, key, name, id) => {
  let updateList = changedlist === null || changedlist.length === 0 ? otherList : otherList.concat({"label": name['name'], "value": id})
  myFirebase.database().ref(`pins/${key}`).update({
    tags: updateList
  })// console.log('######################')
  // console.log(changedlist !== null || changedlist.length !== 0)
  // // console.log(key)
  // // console.log(name['name'])
  // // console.log(id)
  // console.log(updateList)
  // // console.log({"label": name['name'], "value": id})
  // console.log('######################')
}