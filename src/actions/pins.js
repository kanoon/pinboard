import { myFirebase } from "../firebase/firebase";

export const getPins = () => dispatch => {
  myFirebase.database().ref('/').on('value', snapShot => {
    const boardsObject = snapShot.val().boards
    const pinsObject = snapShot.val().pins
    const usersObject = snapShot.val().users
    const tagsObject = snapShot.val().tags
    const boardsList = boardsObject && Object.keys(boardsObject).map(key => ({
      ...boardsObject[key],
      uid: key,
    }));
    const pinsList = pinsObject && Object.keys(pinsObject).map(key => ({
        ...pinsObject[key],
        uid: key,
      }));
    const tagsList = Object.keys(tagsObject).map(key => ({
        ...tagsObject[key],
        uid: key,
      }));
    const usersList = Object.keys(usersObject).map(key => ({
      ...usersObject[key],
      uid: key,
    }));
    dispatch({type: 'GET_PINS', data: {boards: boardsList, pins: pinsList || [], tags: tagsList, users: usersList}})
  })
}

export const createPin = (title, content, command, board, boardId, tags, contributors, clickedButton, currentUser, currentDate) => dispatch => {
  myFirebase.database().ref('pins').push({
    title: title,
    content: content, 
    command: command, 
    board: board,
    boardId: boardId,
    tags: tags,
    contributors: contributors,
    type: clickedButton,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'CREATE_PIN', pin: response})
  })
}

export const udpatePinData = (title, content, command, board, boardId, tags, contributors, clickedButton, currentUser, currentDate, id) => dispatch => {
  myFirebase.database().ref(`pins/${id}`).set({
    title: title,
    content: content,
    command: command,
    board: board,
    boardId: boardId,
    tags: tags,
    contributors: contributors,
    type: clickedButton,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'UPDATE_PIN'})
  })
}

export const deletePin = (id) => dispatch => {
  if(window.confirm('Are you sure ?')){
    myFirebase.database().ref(`pins/${id}`).remove()
   .then((res) => dispatch({type: 'DELETE_PIN', id: id}))
  }
}