import { myFirebase } from "../firebase/firebase";

export const getBoards = () => dispatch => {
  myFirebase.database().ref('/').on('value', snapShot => {
    const boardsObject = snapShot.val().boards
    const usersObject = snapShot.val().users
    const boardsList = boardsObject && Object.keys(boardsObject).map(key => ({
      ...boardsObject[key],
      uid: key,
    }));
    const usersList = Object.keys(usersObject).map(key => ({
      ...usersObject[key],
      uid: key,
    }));
    dispatch({type: 'GET_BOARDS', data: {boards: boardsList || [], users: usersList}})
  })
}

export const createBoard = (title, desc, members, currentUser, currentDate) => dispatch => {
  myFirebase.database().ref('boards').push({
    title: title,
    desc: desc, 
    members: members,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'CREATE_BOARD', board: response})
  })
}

export const udpateBoardData = (title, desc, members, id, currentUser, currentDate) => dispatch => {
  myFirebase.database().ref(`boards/${id}`).set({
    title: title,
    desc: desc, 
    members: members,
    created_by: currentUser.firstName + ' ' + currentUser.lastName,
    created_at: currentDate,
  })
  .then((response) => {
    dispatch({type: 'UPDATE_BOARD'})
  })
}

export const deleteBoard = (id) => dispatch => {
  if(window.confirm('Are you sure ?')){
    const boardPins = []
    myFirebase.database().ref(`pins`).orderByChild("boardId").equalTo(id).on('value', snapShot => {
      snapShot.forEach(item => {
        boardPins.push(item.key)
        // console.log(item.key)
        // console.log(item.val())
      })
      console.log(boardPins)
    })
    boardPins.map((e) => myFirebase.database().ref(`pins/${e}`).remove())
    myFirebase.database().ref(`boards/${id}`).remove()
    .then((res) => dispatch({type: 'DELETE_BOARD', id: id}))
  }
}