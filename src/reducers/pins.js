const initState = {
    boards: [],
    users: [], 
    // pins: [],
    tags: []
  }

  const pins = (state = initState, action) => {
    console.log(action)
    switch (action.type) {
      case 'CREATE_PIN':
        return {
          ...state,
          pins: [...state.pins]
        };
      
      case 'GET_PINS':
        return {
          ...state,
          boards: action.data.boards,
          pins: action.data.pins,
          tags: action.data.tags,
          users: action.data.users
        }
       case 'UPDATE_PIN': {
        return{
          ...state
        }
      }
      case 'DELETE_PIN': {
        const newPinList = state.pins.filter((e) => e.id !== action.id)
        return {
          ...state,
          pins: newPinList
        }
      }
      default:
        return state;
    }
    return state
  }
  
  export default pins;
  