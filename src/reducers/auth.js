import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  VERIFY_REQUEST,
  VERIFY_SUCCESS, 
  PASSWORD_REQUEST_SUCCESS,
  PASSWORD_REQUEST_FAILURE,
  USER_CREATED_SUCCESSFULLY,
  USER_CREATION_FAILURE,
  GET_DATA
} from "../actions/";

export default (
  state = {
    isLoggingIn: false,
    isLoggingOut: false,
    isVerifying: false,
    loginError: false,
    signinError: false,
    logoutError: false,
    isAuthenticated: false,
    passwordRequest: {
      email: "",
      error: "",
      linkReceived: false
    },
    user: {},
    data: {}
    // boards: [],
    // pins: [],
    // users: [],
    // tags: [],
  },
  action
) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoggingIn: true,
        loginError: false
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggingIn: false,
        isAuthenticated: true,
        user: action.user,
        data: action.data
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoggingIn: false,
        isAuthenticated: false,
        loginError: true
      };
    case LOGOUT_REQUEST:
      return {
        ...state,
        isLoggingOut: true,
        logoutError: false
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isLoggingOut: false,
        isAuthenticated: false,
        user: {}
      };
    case LOGOUT_FAILURE:
      return {
        ...state,
        isLoggingOut: false,
        logoutError: true
      };
    case VERIFY_REQUEST:
      return {
        ...state,
        isVerifying: true,
        verifyingError: false
      };
    case VERIFY_SUCCESS:
      return {
        ...state,
        isVerifying: false
      };
    case PASSWORD_REQUEST_SUCCESS:
      return {
        ...state,
        passwordRequest: {linkReceived: true, email: action.email}
      };
    case PASSWORD_REQUEST_FAILURE:
      return {
        ...state,
        passwordRequest: {linkReceived: false, email: action.email, error: action.error}
      };
    case USER_CREATED_SUCCESSFULLY:
      return {
        ...state,
        user: action.user,
        isAuthenticated: true
      };
    case USER_CREATION_FAILURE:
      return {
        ...state,
        signinError: action.error
      }
    case GET_DATA:
      return{
        ...state,
        data: action.data,
        users: action.data.users,
        boards: action.data.boards,
        pins: action.data.pins,
        tags: action.data.tags
      }
    default:
      return state;
  }
};
