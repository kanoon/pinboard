  const initState = {
    // boards: [],
    users: []
  }

  const boards = (state = initState, action) => {
    console.log(action)
    switch (action.type) {
      case 'CREATE_BOARD':
        return {
          ...state,
          boards: [...state.boards]
        };
      
      case 'GET_BOARDS':
        return {
          ...state,
          boards: action.data.boards,
          users: action.data.users
        }
       case 'UPDATE_BOARD': {
        return{
          ...state
        }
      }
      case 'DELETE_BOARD': {
        const newBoardList = state.boards.filter((e) => e.id !== action.id)
        return {
          ...state,
          boards: newBoardList
        }
      }
      default:
        return state;
    }
    return state
  }
  
  export default boards;
  