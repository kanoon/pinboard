const initState = {
    tags: [],
    users: []
  }

  const pins = (state = initState, action) => {
    console.log(action)
    switch (action.type) {
      case 'CREATE_TAG':
        return {
          ...state,
          tags: [...state.tags]
        };
      
      case 'GET_TAGS':
        return {
          ...state,
          tags: action.data.tags,
          users: action.data.users
        }
       case 'UPDATE_TAG': {
        return{
          ...state
        }
      }
      case 'DELETE_TAG': {
        const newTagList = state.tags.filter((e) => e.id !== action.id)
        return {
          ...state,
          tags: newTagList
        }
      }
      default:
        return state;
    }
    return state
  }
  
  export default pins;
  