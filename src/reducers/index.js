import { combineReducers } from "redux";
import auth from "./auth";
import boards from "./boards";
import pins from "./pins";
import tags from "./tags";


export default combineReducers({ auth, boards, pins, tags });
